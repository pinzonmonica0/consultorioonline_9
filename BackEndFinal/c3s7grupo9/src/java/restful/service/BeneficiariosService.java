/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.BeneficiariosModel;
import restful.modelo.Conexion;


public class BeneficiariosService {
   public ArrayList<BeneficiariosModel> obtenerBeneficiarios(){  //cambiar a beneficiarios
        ArrayList<BeneficiariosModel> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        String sql="SELECT * FROM beneficiarios";
        
        try{
            Statement stm = conexion.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
            BeneficiariosModel beneficiario= new BeneficiariosModel();
            beneficiario.setId_beneficiario(rs.getString("Id_beneficiario"));
            beneficiario.setTipo_documento(rs.getString("tipo_documento"));
            beneficiario.setDocumento(rs.getString("documento")); 
            beneficiario.setNombre(rs.getString("nombre"));
            beneficiario.setApellido(rs.getString("apellido"));
            beneficiario.setUsuario(rs.getString("usuario"));
            beneficiario.setContrasena(rs.getString("contrasena"));
            beneficiario.setTelefono1(rs.getString("telefono1"));
            beneficiario.setTelefono2(rs.getString("telefono2"));
            beneficiario.setEmail1(rs.getString("email1"));
            beneficiario.setEmail2(rs.getString("email2"));
            beneficiario.setEstado(rs.getString("estado"));
            lista.add(beneficiario);
                   
            }
        }catch (Exception e){
        }
        conexion.desconectar();
        return lista;
    }
   
   public BeneficiariosModel obtenerbeneficiario(String id){  //cambiar a beneficiarios, 2 METODOS,b Y B
       BeneficiariosModel beneficiario = new  BeneficiariosModel();
        
        Conexion conexion = new Conexion();
        String sql="SELECT * FROM beneficiarios WHERE id_beneficiario = ?";
        
        try{
            PreparedStatement pstm = conexion.getCon().prepareStatement(sql);
            pstm.setString(1,id);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
            
            beneficiario.setId_beneficiario(rs.getString("id_beneficiario"));
            beneficiario.setTipo_documento(rs.getString("tipo_documento"));
            beneficiario.setDocumento(rs.getString("documento")); 
            beneficiario.setNombre(rs.getString("nombre"));
            beneficiario.setApellido(rs.getString("apellido"));
            beneficiario.setUsuario(rs.getString("usuario"));
            beneficiario.setContrasena(rs.getString("contrasena"));
            beneficiario.setTelefono1(rs.getString("telefono1"));
            beneficiario.setTelefono2(rs.getString("telefono2"));
            beneficiario.setEmail1(rs.getString("email1"));
            beneficiario.setEmail2(rs.getString("email2"));
            beneficiario.setEstado(rs.getString("estado"));
            
        }
    }catch (Exception e){
    }
    conexion.desconectar();
    return beneficiario;
  }
     public String eliminarbeneficiario(String id){
         
         Conexion conexion= new Conexion();
         String sql= "DELETE FROM beneficiarios WHERE id_beneficiario = ? ";
         try {
             PreparedStatement pstm = conexion.getCon().prepareStatement(sql);
             pstm.setString(1,id);
             pstm.executeUpdate();
         }catch (Exception e) {
             return "{\"Accion\":\"Error\"}";
             
             
         }
        conexion.desconectar();
         return  "{\"Accion\":\"Registro borrado\"}";
     }
     
     
      public BeneficiariosModel insertarBeneficiario(BeneficiariosModel beneficiario){
        Conexion conexion = new Conexion();
        String sql = "INSERT INTO beneficiarios values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
              
             try{
            PreparedStatement pstm = conexion.getCon().prepareStatement(sql);
            pstm.setString(1, beneficiario.getId_beneficiario());
            pstm.setString(2, beneficiario.getTipo_documento());
            pstm.setString(3, beneficiario.getDocumento());
            pstm.setString(4, beneficiario.getNombre());
            pstm.setString(5, beneficiario.getApellido());
            pstm.setString(6, beneficiario.getUsuario());
            pstm.setString(7, beneficiario.getContrasena());
            pstm.setString(8, beneficiario.getTelefono1());
            pstm.setString(9, beneficiario.getTelefono2());
            pstm.setString(10, beneficiario.getEmail1());
            pstm.setString(11, beneficiario.getEmail2());
            pstm.setString(12, " ");
            pstm.setString(13, beneficiario.getEstado());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return null;
        }return beneficiario;
      }
    
      
      public BeneficiariosModel modificarBeneficiario(BeneficiariosModel beneficiario){
        Conexion conexion = new Conexion();
        String sql = "UPDATE beneficiarios SET tipo_documento=?,documento=?,nombre=?,apellido=?,usuario=?,contrasena=?,telefono1=?,telefono2=?,email1=?,email2=?,estado=? WHERE id_beneficiario=?";
        
        try {
            PreparedStatement pstm = conexion.getCon().prepareStatement(sql);  
            
            pstm.setString(1, beneficiario.getTipo_documento());
            pstm.setString(2, beneficiario.getDocumento());
            pstm.setString(3, beneficiario.getNombre());
            pstm.setString(4, beneficiario.getApellido());
            pstm.setString(5, beneficiario.getUsuario());
            pstm.setString(6, beneficiario.getContrasena());
            pstm.setString(7, beneficiario.getTelefono1());
            pstm.setString(8, beneficiario.getTelefono2());
            pstm.setString(9, beneficiario.getEmail1());
            pstm.setString(10, beneficiario.getEmail2());
            pstm.setString(11, beneficiario.getEstado());
            pstm.setString(12, beneficiario.getId_beneficiario());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return null;
        }
         
        return beneficiario;
    }
      
     public String validarBeneficiario(String usuario, String clave){  //validar si el usuario clave corresponden a un beneficiario, si corresponde entregar el Id de beneficiario
        String IdBeneficiario = "";
        
        Conexion conexion = new Conexion();
        String sql="SELECT id_beneficiario FROM beneficiarios WHERE usuario =? AND contrasena=?";
        
        try{
            PreparedStatement pstm = conexion.getCon().prepareStatement(sql);
            pstm.setString(1,usuario);
            pstm.setString(2,clave);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                
                IdBeneficiario=rs.getString("id_beneficiario");
                        
            }
        }catch (Exception e){
            return "";
    }
    conexion.desconectar();
    return IdBeneficiario;
  }
}
