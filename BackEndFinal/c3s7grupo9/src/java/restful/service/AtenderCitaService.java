/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import restful.modelo.AtenderCitaModel;
import restful.modelo.CitasAsignadasModel;
import restful.modelo.Conexion;
import restful.modelo.FormulaModel;
import restful.modelo.ItemFormulaModel;

/**
 *
 * @author PC
 */
public class AtenderCitaService {
    
    public ArrayList<CitasAsignadasModel> obtenerCitasAsignadasMedico(String id){
        ArrayList<CitasAsignadasModel> lista = new ArrayList<>();
        Conexion conex = new Conexion();
        String sql = "SELECT A.IdCita,"
                + "CONCAT_WS('',B.nombres ,B.apellidos) as nombre, A.FechaHora,"
                + "CONCAT_WS('',C.nombre ,C.apellido) as paciente"
                + " FROM agenda_citas A INNER JOIN usuarios_del_sistema B ON A.Medico=B.Id_usuario"
                + " INNER JOIN beneficiarios C ON A.Beneficiario=C.id_beneficiario"
                + " WHERE NOT A.Beneficiario IS NULL AND A.Diagnostico IS NULL AND A.Medico=? ORDER BY A.FechaHora";
                
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                
                CitasAsignadasModel fila = new CitasAsignadasModel();
                fila.setId_cita(rs.getInt("IdCita"));
                fila.setMedico(rs.getString("nombre"));
                fila.setFechahora(rs.getString("FechaHora"));
                fila.setBeneficiario(rs.getString("paciente"));
                
                lista.add(fila);
                     
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return lista;
    }
    
    public String atenderCita(AtenderCitaModel cita){
        Conexion conex = new Conexion();
        String sql = "UPDATE agenda_citas SET Diagnostico=?,FechaHoraAtencion=? WHERE IdCita=? AND NOT Beneficiario IS NULL AND Diagnostico IS NULL";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);  
            
            pstm.setString(1, cita.getDiagnostico());
            pstm.setDate(2, Date.valueOf(LocalDate.now()));
            pstm.setInt(3, cita.getId_cita());   
            
            
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return "{\"Accion\":\"Error\"}";
        }
        
        FormulaModel objFormula= cita.getFormula();
        for (ItemFormulaModel cadena: objFormula.getMedicamento()) 
        {
            
            String sql2 = "INSERT INTO formulas_medicas values(?,?,?,?)";
        
            try {
                PreparedStatement pstm2 = conex.getCon().prepareStatement(sql2);
                pstm2.setInt(1, cita.getId_cita());
                pstm2.setString(2, cadena.getCodigo());
                pstm2.setDouble(3, cadena.getCantidad());
                pstm2.setString(4, cadena.getInstrucciones());
                
                pstm2.executeUpdate();
            } catch (Exception e) {
                System.out.println("e = " + e.getMessage());
                return "{\"Accion\":\"Error\"}";
            }
                       
        }
       
        return "{\"Accion\":\"Cita Atendida\"}";
    }
    
}
