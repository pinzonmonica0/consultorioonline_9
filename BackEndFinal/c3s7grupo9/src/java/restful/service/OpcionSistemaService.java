/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.OpcionSistemaModel;


/**
 *
 * @author frendon
 */
public class OpcionSistemaService {
    public ArrayList<OpcionSistemaModel> obtenerOpcionesSistema() {
        ArrayList<OpcionSistemaModel> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT * FROM opciones_sistema";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                OpcionSistemaModel opcion = new OpcionSistemaModel();
                opcion.setCodigo(rs.getString("Codigo"));
                opcion.setDescripcion(rs.getString("Descripcion"));
                opcion.setPagina(rs.getString("Pagina"));
                opcion.setDestino(rs.getString("Destino"));
                opcion.setNombreWeb(rs.getString("NombreWeb"));
                lista.add(opcion);
            }
        } catch (Exception e) {
        }
        conex.desconectar();
        return lista;
    }
    
    
     public OpcionSistemaModel obtenerOpcionSistema(String id){
        OpcionSistemaModel opcion = new OpcionSistemaModel();
        Conexion conex = new Conexion();
        String sql  = "SELECT * FROM opciones_sistema WHERE Codigo = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                opcion.setCodigo(rs.getString("Codigo"));
                opcion.setDescripcion(rs.getString("Descripcion"));
                opcion.setPagina(rs.getString("Pagina"));
                opcion.setDestino(rs.getString("Destino"));
                opcion.setNombreWeb(rs.getString("NombreWeb"));
                               
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return opcion;
    }
     
     
    public String eliminarOpcionSistema(String id)
    {
        Conexion conex = new Conexion();
        String sql = "DELETE FROM opciones_sistema WHERE Codigo = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            return "{\"Accion\":\"Error\"}";
        }
        
        
        conex.desconectar();
        return "{\"Accion\":\"Registro Borrado\"}";
    }
    
    public OpcionSistemaModel insertarOpcionSistema(OpcionSistemaModel opcion){
        Conexion conex = new Conexion();
        String sql = "INSERT INTO opciones_sistema values(?,?,?,?,?)";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            
            pstm.setString(1, opcion.getCodigo());
            pstm.setString(2, opcion.getDescripcion());
            pstm.setString(2, opcion.getPagina());
            pstm.setString(2, opcion.getDestino());
            pstm.setString(2, opcion.getNombreWeb());
            
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
        
        conex.desconectar();
        return opcion;
    }
    
    public OpcionSistemaModel modificarOpcionSistema(OpcionSistemaModel opcion){
        Conexion conex = new Conexion();
        String sql = "UPDATE opciones_sistema SET Descripcion=?, Pagina=?, Destino=?,NombreWeb=' WHERE Codigo=?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);  
            
            pstm.setString(1, opcion.getCodigo());
            pstm.setString(2, opcion.getDescripcion());
            pstm.setString(2, opcion.getPagina());
            pstm.setString(2, opcion.getPagina());
            pstm.setString(2, opcion.getNombreWeb());
            
            
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
        
        conex.desconectar();
        return opcion;
    } 
    
}
