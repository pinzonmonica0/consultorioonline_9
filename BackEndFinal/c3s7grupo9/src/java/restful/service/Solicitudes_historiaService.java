/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.Solicitudes_historiaModel;


public class Solicitudes_historiaService {
    public ArrayList<Solicitudes_historiaModel>obtenerSolicitudesHistoria(){
        ArrayList<Solicitudes_historiaModel> lista= new ArrayList<>();
        
        Conexion conexion = new Conexion();
        String sql= "SELECT * FROM Solicitudes_Historia";
        
        try{
            Statement stm= conexion.getCon().createStatement();
            ResultSet rs= stm.executeQuery(sql);
            while(rs.next()){
              Solicitudes_historiaModel historia= new Solicitudes_historiaModel();
              historia.setBeneficiario(rs.getString("beneficiario"));
              historia.setFechaHora(rs.getString("fechaHora"));
              historia.setUsuario(rs.getString("usuario"));
              historia.setEstado(rs.getString("estado"));
              historia.setFechaCambioEstado(rs.getString("fechaCambioEstado"));
              lista.add(historia);
        }
            
        }catch(Exception e){
        }
        return lista;
    }
    
    public Solicitudes_historiaModel obtenerSolicitudHistoria(String id){
        Solicitudes_historiaModel historia= new Solicitudes_historiaModel();
        Conexion conexion= new Conexion();
        String sql="SELECT * FROM Solicitudes_Historia WHERE beneficiario = ?";
        
        try{
            PreparedStatement pstm= conexion.getCon().prepareStatement(sql);
            pstm.setString(1,id);
            ResultSet rs= pstm.executeQuery();
            while(rs.next()){
              
              historia.setBeneficiario(rs.getString("beneficiario"));
              historia.setFechaHora(rs.getString("fechaHora"));
              historia.setUsuario(rs.getString("usuario"));
              historia.setEstado(rs.getString("estado"));
              historia.setFechaCambioEstado(rs.getString("fechaCambioEstado"));
             
         }
    }catch (Exception e){
    }
        
    return historia;
  }
       public Solicitudes_historiaModel insertarhistoria(Solicitudes_historiaModel historia){
        Conexion conexion = new Conexion();
        String sql = "INSERT INTO Solicitudes_Historia values(?,?,?,?,?)";
        
        try {
            PreparedStatement pstm = conexion.getCon().prepareStatement(sql);
            pstm.setString(1, historia.getBeneficiario());
            pstm.setString(2, historia.getFechaHora());
            pstm.setString(3, historia.getUsuario());
            pstm.setString(4, historia.getEstado());
            pstm.setString(5, historia.getFechaCambioEstado());
           
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
         return historia;
    }
    
    public Solicitudes_historiaModel modificarhistoria(Solicitudes_historiaModel historia){
        Conexion conexion = new Conexion();
        String sql = "UPDATE Solicitudes_Historia  SET fechaHora=?,usuario=?,estado=?,fechaCambioEstado=? WHERE beneficiario=?";
        
        try {
            PreparedStatement pstm = conexion.getCon().prepareStatement(sql);  
            
            pstm.setString(1, historia.getBeneficiario());
            pstm.setString(2, historia.getFechaHora());
            pstm.setString(3, historia.getUsuario());
            pstm.setString(4, historia.getEstado());
            pstm.setString(5, historia.getFechaCambioEstado());
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
        
        return historia;
    }
}
