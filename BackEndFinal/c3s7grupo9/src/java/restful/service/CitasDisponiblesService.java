/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.CitasDisponiblesModel;
import restful.modelo.Conexion;
import restful.modelo.ReservaCitaModel;

/**
 *
 * @author PC
 */
public class CitasDisponiblesService {
   public ArrayList<CitasDisponiblesModel> obtenerCitasDisponibles() {
        ArrayList<CitasDisponiblesModel> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT C.IdCita,"
                + "CONCAT_WS('',B.nombres ,B.apellidos) as nombre, C.FechaHora "
                + " FROM agenda_citas C INNER JOIN usuarios_del_sistema B ON C.Medico=B.Id_usuario"
                + " WHERE C.Beneficiario IS NULL";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                CitasDisponiblesModel fila = new CitasDisponiblesModel();
                fila.setId_cita(rs.getInt("IdCita"));
                fila.setMedico(rs.getString("nombre"));
                fila.setFechahora(rs.getString("FechaHora"));
                
                lista.add(fila);
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return lista;
    }
    
    public String reservarCita(ReservaCitaModel cita){
        Conexion conex = new Conexion();
        String sql = "UPDATE agenda_citas SET Beneficiario=? WHERE IdCita=?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);  
            
            pstm.setString(1, cita.getBeneficiario());
            pstm.setInt(2, cita.getId_cita());   
            
            
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return "{\"Accion\":\"Error\"}";
        }
        
        return "{\"Accion\":\"Cita Reservada\"}";
    }
   
}
