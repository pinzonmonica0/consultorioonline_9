/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.Informe2Model;


/**
 *
 * @author frendon
 */
public class Informe2Service {
    public ArrayList<Informe2Model> obtenerInforme() {
        ArrayList<Informe2Model> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT concat_ws('',B.id_beneficiario,B.tipo_documento) AS documento,"
                + "CONCAT_WS(' ',B.nombre ,B.apellido) as nombre,B.telefono1,B.email1,"
                + "CONCAT_WS(' ',U.nombres ,U.apellidos) as medico,C.FechaHoraAtencion"
                + " FROM Agenda_citas C INNER JOIN beneficiarios B ON C.Beneficiario=B.id_beneficiario"
                + " INNER JOIN usuarios_del_sistema U ON C.Medico=U.Id_usuario"
                + " WHERE NOT C.FechaHoraAtencion IS NULL";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Informe2Model fila = new Informe2Model();
                fila.setDocumento_beneficiario(rs.getString("documento"));
                fila.setNombre_beneficiario(rs.getString("nombre"));
                fila.setTelefono(rs.getString("telefono1"));
                fila.setDireccion(rs.getString("email1"));
                fila.setNombre_medico(rs.getString("medico"));
                fila.setFecha_cita(rs.getDate("FechaHoraAtencion"));
                lista.add(fila);
            }
        } catch (Exception e) {
        }
        conex.desconectar();
        return lista;
    }
    
}
