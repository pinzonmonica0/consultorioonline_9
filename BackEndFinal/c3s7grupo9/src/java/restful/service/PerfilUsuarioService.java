/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.PerfilUsuarioModel;


/**
 *
 * @author frendon
 */
public class PerfilUsuarioService {
    
    public ArrayList<PerfilUsuarioModel> obtenerPerfiles() {
        ArrayList<PerfilUsuarioModel> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT * FROM perfiles_usuario";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                PerfilUsuarioModel perfil = new PerfilUsuarioModel();
                perfil.setIdPerfil(rs.getString("idPerfil"));
                perfil.setDescripcion(rs.getString("descripcion"));
                lista.add(perfil);
            }
        } catch (Exception e) {
        }
        conex.desconectar();
        return lista;
    }
    
    
     public PerfilUsuarioModel obtenerPerfil(String id){
        PerfilUsuarioModel perfil = new PerfilUsuarioModel();
        Conexion conex = new Conexion();
        String sql  = "SELECT * FROM perfiles_usuario WHERE idPerfil = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                perfil.setIdPerfil(rs.getString("idPerfil"));
                perfil.setDescripcion(rs.getString("descripcion"));
                               
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return perfil;
    }
     
     
    public String eliminarPerfil(String id)
    {
        Conexion conex = new Conexion();
        String sql = "DELETE FROM perfiles_usuario WHERE idPerfil = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            return "{\"Accion\":\"Error\"}";
        }
        
        
        conex.desconectar();
        return "{\"Accion\":\"Registro Borrado\"}";
    }
    
    public PerfilUsuarioModel insertarPerfil(PerfilUsuarioModel perfil){
        Conexion conex = new Conexion();
        String sql = "INSERT INTO perfiles_usuario values(?,?)";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            
            pstm.setString(1, perfil.getIdPerfil());
            pstm.setString(2, perfil.getDescripcion());
            
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
        
        conex.desconectar();
        return perfil;
    }
    
    public PerfilUsuarioModel modificarPerfil(PerfilUsuarioModel perfil){
        Conexion conex = new Conexion();
        String sql = "UPDATE perfiles_usuario SET descripcion=? WHERE idPerfil=?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);  
            
            pstm.setString(1, perfil.getDescripcion());
            pstm.setString(2, perfil.getIdPerfil());
            
            
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
        
        conex.desconectar();
        return perfil;
    } 
    
}
