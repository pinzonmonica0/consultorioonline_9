/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import restful.modelo.Conexion;
import restful.modelo.FormulaModel;
import restful.modelo.Informe3Model;
import restful.modelo.ItemFormulaModel;

/**
 *
 * @author frendon
 */
public class Informe3Service {
    public ArrayList<Informe3Model> obtenerInforme() {
        ArrayList<Informe3Model> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT C.IdCita,concat_ws('',B.id_beneficiario,B.tipo_documento) AS documento,"
                + "CONCAT_WS(' ',B.nombre ,B.apellido) as nombre,B.telefono1,B.email1,"
                + "CONCAT_WS(' ',U.nombres ,U.apellidos) as medico,C.FechaHoraAtencion,C.Diagnostico"
                + " FROM Agenda_citas C INNER JOIN beneficiarios B ON C.Beneficiario=B.id_beneficiario"
                + " INNER JOIN usuarios_del_sistema U ON C.Medico=U.Id_usuario"
                + " WHERE NOT C.FechaHoraAtencion IS NULL";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Informe3Model fila = new Informe3Model();
                fila.setDocumento_beneficiario(rs.getString("documento"));
                fila.setNombre_beneficiario(rs.getString("nombre"));
                fila.setTelefono(rs.getString("telefono1"));
                fila.setDireccion(rs.getString("email1"));
                fila.setNombre_medico(rs.getString("medico"));
                fila.setFecha_cita(rs.getDate("FechaHoraAtencion"));
                fila.setDiagnostico(rs.getString("Diagnostico"));
                
                
                String sql2 = "SELECT B.Codigo_medicamento,B.Nombre_generico, B.Nombre_Comercial, B.Presentacion,A.Cantidad,A.Instrucciones"                       
                + " FROM Formulas_Medicas A INNER JOIN medicamentos B ON A.Id_medicamento=B.Codigo_medicamento"
                + " WHERE A.IdCita=" + rs.getString("IdCita");
                
                FormulaModel objFormula=new FormulaModel();
                ArrayList<ItemFormulaModel> medicamentos=new ArrayList<>();
                
                //ResultSet rsf = stm.executeQuery(sql2);
                PreparedStatement pstm = conex.getCon().prepareStatement(sql2);
                //pstm.setString(1, rs.getString("IdCita"));
                ResultSet rsf = pstm.executeQuery();
                while (rsf.next()) {
                    
                    ItemFormulaModel medicamento = new ItemFormulaModel();
                    
                    medicamento.setCodigo(rsf.getString("Codigo_medicamento"));
                    medicamento.setNombre_generico(rsf.getString("Nombre_generico"));
                    medicamento.setNombre_comercial(rsf.getString("Nombre_Comercial"));
                    medicamento.setInstrucciones(rsf.getString("Instrucciones"));
                    medicamento.setCantidad(rsf.getDouble("Cantidad"));
                    
                    medicamentos.add(medicamento);
                }
                
                objFormula.setMedicamento(medicamentos);
                fila.setFormula(objFormula);
                
                               
                lista.add(fila);
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return lista;
    }
}
