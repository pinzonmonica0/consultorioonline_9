/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.MedicamentosModel;


public class MedicamentosService {
    public ArrayList<MedicamentosModel> ObtenerMedicamentos(){
        ArrayList<MedicamentosModel> lista = new ArrayList<>();
        
        Conexion conex = new Conexion ();
        String sql = "SELECT * FROM medicamentos";
        
        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            
            
            while (rs.next()){
                MedicamentosModel medicamentos = new MedicamentosModel();
                medicamentos.setCodigo_medicamento(rs.getString("Codigo_medicamento"));
                medicamentos.setNombre_generico(rs.getString("Nombre_generico"));
                medicamentos.setNombre_Comercial(rs.getString("Nombre_Comercial"));
                medicamentos.setLaboratorio(rs.getString("laboratorio"));
                medicamentos.setEstado(rs.getString("Estado"));
                medicamentos.setPresentacion(rs.getString("Presentacion"));
                lista.add(medicamentos);
            }
            
            
        } catch(Exception e){
        }
        return lista;
    }
    
    public MedicamentosModel obtnerMedicamentos(String id){
        MedicamentosModel medicamentos = new MedicamentosModel();
        Conexion conex = new Conexion();
        String sql = "SELECT * FROM medicamentos WHERE codigo_medicamento = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            
            while (rs.next()){
                medicamentos.setCodigo_medicamento(rs.getString("Codigo_medicamento"));
                medicamentos.setNombre_generico(rs.getString("nombre_generico"));
                medicamentos.setNombre_Comercial(rs.getString("nombre_Comercial"));
                medicamentos.setLaboratorio(rs.getString("laboratorio"));
                medicamentos.setEstado(rs.getString("Estado"));
                medicamentos.setPresentacion(rs.getString("Presentacion"));
            }
            
            
        } catch(Exception e) {
        }
        return medicamentos;
        
    }
    
    public String eliminarMedicamentos(String id)
    {
        Conexion conex =new Conexion();
        String sql = "DELETE FROM medicamentos WHERE codigo_medicamento = ?";
       
        try {
            PreparedStatement pstm =conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            return "{\"Accion\":\"Error\"}";
        }
       
       
        conex.desconectar();
        return"{\"Accion\":\"Registro borrado\"}";
    }
    
    public MedicamentosModel insertarMedicamentos(MedicamentosModel medicamentos) {
        Conexion conex = new Conexion();
        String sql = "INSERT INTO medicamentos values(?,?,?,?,?)";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, medicamentos.getCodigo_medicamento());
            pstm.setString(2, medicamentos.getNombre_generico());
            pstm.setString(3, medicamentos.getNombre_Comercial());
            pstm.setString(4, medicamentos.getLaboratorio());
            pstm.setString(5, medicamentos.getEstado());
            pstm.setString(6, medicamentos.getPresentacion());
            
            
        } catch (Exception e) {
            
            return medicamentos;
        }
        
        return medicamentos;
    }
    
}
