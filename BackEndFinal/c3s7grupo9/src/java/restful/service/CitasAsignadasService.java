/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.CitasAsignadasModel;
import restful.modelo.Conexion;
import restful.modelo.ReservaCitaModel;

/**
 *
 * @author PC
 */
public class CitasAsignadasService {
    public ArrayList<CitasAsignadasModel> obtenerCitasAsignadas() {
        ArrayList<CitasAsignadasModel> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT A.IdCita,"
                + "CONCAT_WS('',B.nombres ,B.apellidos) as nombre, A.FechaHora,"
                + "CONCAT_WS('',C.nombre ,C.apellido) as paciente"
                + " FROM agenda_citas A INNER JOIN usuarios_del_sistema B ON A.Medico=B.Id_usuario"
                + " INNER JOIN beneficiarios C ON A.Beneficiario=C.id_beneficiario"
                + " WHERE NOT A.Beneficiario IS NULL AND A.Diagnostico IS NULL ORDER BY A.FechaHora";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                CitasAsignadasModel fila = new CitasAsignadasModel();
                fila.setId_cita(rs.getInt("IdCita"));
                fila.setMedico(rs.getString("nombre"));
                fila.setFechahora(rs.getString("FechaHora"));
                fila.setBeneficiario(rs.getString("paciente"));
                
                lista.add(fila);
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return lista;
    }
    
    public ArrayList<CitasAsignadasModel> obtenerCitasAsignadasBeneficiario(String id){
        ArrayList<CitasAsignadasModel> lista = new ArrayList<>();
        Conexion conex = new Conexion();
        String sql = "SELECT A.IdCita,"
                + "CONCAT_WS('',B.nombres ,B.apellidos) as nombre, A.FechaHora,"
                + "CONCAT_WS('',C.nombre ,C.apellido) as paciente"
                + " FROM agenda_citas A INNER JOIN usuarios_del_sistema B ON A.Medico=B.Id_usuario"
                + " INNER JOIN beneficiarios C ON A.Beneficiario=C.id_beneficiario"
                + " WHERE NOT A.Beneficiario IS NULL AND A.Diagnostico IS NULL AND A.Beneficiario=? ORDER BY A.FechaHora";
                
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                
                CitasAsignadasModel fila = new CitasAsignadasModel();
                fila.setId_cita(rs.getInt("IdCita"));
                fila.setMedico(rs.getString("nombre"));
                fila.setFechahora(rs.getString("FechaHora"));
                fila.setBeneficiario(rs.getString("paciente"));
                
                lista.add(fila);
                     
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return lista;
    }
    
    public String cancelarCita(ReservaCitaModel cita){
        Conexion conex = new Conexion();
        String sql = "UPDATE agenda_citas SET Beneficiario=NULL WHERE IdCita=? AND NOT Beneficiario IS NULL AND Diagnostico IS NULL";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);  
            
            pstm.setInt(1, cita.getId_cita());   
            
            
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return "{\"Accion\":\"Error\"}";
        }
        
        return "{\"Accion\":\"Cita Cancelada\"}";
    }
    
    
}
