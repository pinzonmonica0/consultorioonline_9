/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.AtenderCitaModel;
import restful.modelo.CitasAsignadasModel;
import restful.modelo.ReservaCitaModel;
import restful.service.AtenderCitaService;

/**
 * REST Web Service
 *
 * @author PC
 */
@Path("AtenderCitas")
public class AtenderCitaResource {
    AtenderCitaService citas = new AtenderCitaService();

    @Path("/{MedicoId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CitasAsignadasModel> getCitasAsignadasMedico(@PathParam("MedicoId") String id){
        return citas.obtenerCitasAsignadasMedico(id);
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String atenderCita(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        AtenderCitaModel modelo = gson.fromJson(JSON, AtenderCitaModel.class);
        try {
        return citas.atenderCita(modelo);
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return "{\"Accion\":\"Error\"}";
        }
    }
}
