/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import restful.modelo.CitasDisponiblesModel;
import restful.modelo.ReservaCitaModel;
import restful.service.CitasDisponiblesService;


@Path("CitasDisponibles")
public class CitasDisponiblesResource {

    CitasDisponiblesService citas = new CitasDisponiblesService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CitasDisponiblesModel> getCitas() {
        
        return citas.obtenerCitasDisponibles();
    }
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String reservaCita(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        ReservaCitaModel modelo = gson.fromJson(JSON, ReservaCitaModel.class);
        
        return citas.reservarCita(modelo);
    }
}
