/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.TipoDocumentoModel;
import restful.service.TipoDocumentoService;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("TipoDocumento")
public class TipoDocumentoResource {

    TipoDocumentoService tipoDocumento = new TipoDocumentoService();
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<TipoDocumentoModel> getTiposDocumento() {
        
        return tipoDocumento.obtenerTiposDocumento();
    }

    @Path("/{TipoDocumentoId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public TipoDocumentoModel getTipoDocumento(@PathParam("TipoDocumentoId") String id){
        return tipoDocumento.obtenerTipoDocumento(id);
    }

    
}
