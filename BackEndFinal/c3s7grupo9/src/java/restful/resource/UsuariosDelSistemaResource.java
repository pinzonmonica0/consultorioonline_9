
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.UsuariosDelSistemaModel;
import restful.service.UsuariosDelSistemaService;

@Path("Usuarios")
public class UsuariosDelSistemaResource {
    UsuariosDelSistemaService usuarios = new UsuariosDelSistemaService();

    
        
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<UsuariosDelSistemaModel> getUsuarios() {
       
        return usuarios.obtenerUsuarios();
    }
    
    @Path("/(UsuariosId)")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UsuariosDelSistemaModel getUsuarios(@PathParam("UsuariosId")String id) throws SQLException{
        return usuarios.obtenerUsuario(id);
    }
    @DELETE
    @Path("/{UsuariosId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String delUsuarios(@PathParam("UsuariosId") String id)
    {
        return "Registro eliminado codigo = " + id;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UsuariosDelSistemaModel addUsuarios (String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
       UsuariosDelSistemaModel modelo = gson.fromJson(JSON,UsuariosDelSistemaModel.class);
        return usuarios.insertarUsuarios(modelo);
        
    }
             
}
        
       
    



    