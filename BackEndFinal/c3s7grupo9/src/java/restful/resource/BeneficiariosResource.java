/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.BeneficiariosModel;
import restful.service.BeneficiariosService;


@Path("Beneficiarios")
public class BeneficiariosResource {

    BeneficiariosService beneficiario = new BeneficiariosService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<BeneficiariosModel> getBeneficiarios() {
       
        
        return beneficiario.obtenerBeneficiarios();
    }
    
    @Path("/{beneficiarioid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BeneficiariosModel getBeneficiario(@PathParam("beneficiarioid")String id){
        return beneficiario.obtenerbeneficiario(id);
    }
    
    
    @DELETE
    @Path("/{beneficiarioid}")
    @Produces(MediaType.APPLICATION_JSON)
    public String delCliente(@PathParam("beneficiarioid") String id)
    {
        return beneficiario.eliminarbeneficiario(id);
    }
        
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)    
    public BeneficiariosModel addCBeneficiario(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        BeneficiariosModel modelo = gson.fromJson(JSON, BeneficiariosModel.class);
       
        return beneficiario.insertarBeneficiario(modelo);
           
    }
    
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BeneficiariosModel updateBeneficiario(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        BeneficiariosModel modelo = gson.fromJson(JSON,BeneficiariosModel.class);
        
        return beneficiario.modificarBeneficiario(modelo);
    
    }
    
}
