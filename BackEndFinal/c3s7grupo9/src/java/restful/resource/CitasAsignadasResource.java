/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.CitasAsignadasModel;
import restful.modelo.ReservaCitaModel;
import restful.service.CitasAsignadasService;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("CitasAsignadas")
public class CitasAsignadasResource {
    CitasAsignadasService citas = new CitasAsignadasService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CitasAsignadasModel> getCitas() {
        
        return citas.obtenerCitasAsignadas();
    }
    
    @Path("/{BeneficiarioId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CitasAsignadasModel> getCitasAsignadasBeneficiario(@PathParam("BeneficiarioId") String id){
        return citas.obtenerCitasAsignadasBeneficiario(id);
    }
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String cancelarCita(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        ReservaCitaModel modelo = gson.fromJson(JSON, ReservaCitaModel.class);
        
        return citas.cancelarCita(modelo);
    }
    
}
