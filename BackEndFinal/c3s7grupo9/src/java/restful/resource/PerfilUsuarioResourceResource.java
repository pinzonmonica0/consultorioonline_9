/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.PerfilUsuarioModel;
import restful.service.PerfilUsuarioService;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("PerfilUsuario")
public class PerfilUsuarioResourceResource {

    PerfilUsuarioService perfil = new PerfilUsuarioService();
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<PerfilUsuarioModel> getPerfiles() {
        
        return perfil.obtenerPerfiles();
    }

    @Path("/{PerfilId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public PerfilUsuarioModel getTipoDocumento(@PathParam("PerfilId") String id){
        return perfil.obtenerPerfil(id);
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PerfilUsuarioModel addPerfil(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        PerfilUsuarioModel modelo = gson.fromJson(JSON, PerfilUsuarioModel.class);
        
        return perfil.insertarPerfil(modelo);
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PerfilUsuarioModel updatePerfil(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        PerfilUsuarioModel modelo = gson.fromJson(JSON, PerfilUsuarioModel.class);
        
        return perfil.modificarPerfil(modelo);
    }
    
    
}
