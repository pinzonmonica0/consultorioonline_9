/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.Solicitudes_historiaModel;
import restful.service.Solicitudes_historiaService;


@Path("Solicitudes_historia")
public class Solicitudes_historiaResource {
Solicitudes_historiaService historia = new Solicitudes_historiaService();
 
 @GET
 @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Solicitudes_historiaModel> getSolicitudes_historia(){
       
        return historia.obtenerSolicitudesHistoria();
   }
   @Path("/{beneficiario}")
   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public Solicitudes_historiaModel getSolicitud_historia(@PathParam("beneficiario")String id){
       return historia.obtenerSolicitudHistoria(id);
       
   }
   

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Solicitudes_historiaModel addhistoria(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Solicitudes_historiaModel modelo = gson.fromJson(JSON, Solicitudes_historiaModel.class);
        
        return historia.insertarhistoria(modelo);
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Solicitudes_historiaModel updatehistoria(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Solicitudes_historiaModel modelo = gson.fromJson(JSON, Solicitudes_historiaModel.class);
        
        return historia.modificarhistoria(modelo);
    }
    
}
