/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import restful.modelo.Informe3Model;
import restful.service.Informe3Service;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("Informe3")
public class Informe3Resource {

    Informe3Service informe = new Informe3Service();
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Informe3Model> getInforme() {
        
        return informe.obtenerInforme();
    }
}
