/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.MedicamentosModel;
import restful.service.MedicamentosService;


@Path("Medicamentos")
public class MedicamentosResource {
    MedicamentosService medicamentos = new MedicamentosService();

   
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<MedicamentosModel> getMedicamentos() {
        
        return medicamentos.ObtenerMedicamentos();
    }
    
    @Path("/{MedicamentoId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public MedicamentosModel getMedicamento(@PathParam("MedicamentoId")String id) {
        return medicamentos.obtnerMedicamentos(id);
        
    }
    
    @DELETE
    @Path("/{MedicamentosId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String delMedicamento(@PathParam("MedicamentosId")String id) { 
        
        return medicamentos.eliminarMedicamentos(id);
                
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public MedicamentosModel addMedicamento (String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        MedicamentosModel modelo = gson.fromJson(JSON,MedicamentosModel.class);
        return medicamentos.insertarMedicamentos(modelo);
    }   
    
    

    
}
