/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MedicamentosModel {
    private String Codigo_medicamento;
    private String Nombre_generico;
    private String Nombre_Comercial;
    private String Laboratorio;
    private String Estado;
    private String Presentacion;

    public String getCodigo_medicamento() {
        return Codigo_medicamento;
    }

    public void setCodigo_medicamento(String Codigo_medicamento) {
        this.Codigo_medicamento = Codigo_medicamento;
    }

    public String getNombre_generico() {
        return Nombre_generico;
    }

    public void setNombre_generico(String Nombre_generico) {
        this.Nombre_generico = Nombre_generico;
    }

    public String getNombre_Comercial() {
        return Nombre_Comercial;
    }

    public void setNombre_Comercial(String Nombre_Comercial) {
        this.Nombre_Comercial = Nombre_Comercial;
    }

    public String getLaboratorio() {
        return Laboratorio;
    }

    public void setLaboratorio(String Laboratorio) {
        this.Laboratorio = Laboratorio;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getPresentacion() {
        return Presentacion;
    }

    public void setPresentacion(String Presentacion) {
        this.Presentacion = Presentacion;
    }
    
    public MedicamentosModel() {
    }

    public MedicamentosModel(String Codigo_medicamento, String Nombre_generico, String Nombre_comercial, String Laboratorio, String Estado, String Presentacion) {
        this.Codigo_medicamento = Codigo_medicamento;
        this.Nombre_generico = Nombre_generico;
        this.Nombre_Comercial = Nombre_comercial;
        this.Laboratorio = Laboratorio;
        this.Estado = Estado;
        this.Presentacion = Presentacion;
    }
    
}
