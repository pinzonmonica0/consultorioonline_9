
package restful.modelo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author frendon
 */
@XmlRootElement
public class TipoDocumentoModel {
    private String id_tipo_documento;
    private String tipo_de_documento;
    
    
    public TipoDocumentoModel() {
    }

    /**
     * @return the id_tipo_documento
     */
    public String getId_tipo_documento() {
        return id_tipo_documento;
    }

    /**
     * @param id_tipo_documento the id_tipo_documento to set
     */
    public void setId_tipo_documento(String id_tipo_documento) {
        this.id_tipo_documento = id_tipo_documento;
    }

    /**
     * @return the tipo_de_documento
     */
    public String getTipo_de_documento() {
        return tipo_de_documento;
    }

    /**
     * @param tipo_de_documento the tipo_de_documento to set
     */
    public void setTipo_de_documento(String tipo_de_documento) {
        this.tipo_de_documento = tipo_de_documento;
    }
    
    
    
}
