
package restful.modelo;

public class CitasDisponiblesModel {
    private int id_cita;
    private String medico;
    private String fechahora;
//    private String beneficiario;
//    private String diagnostico;
//    private String fechahoraatencion;
    
    public CitasDisponiblesModel() {
    }
    
    public CitasDisponiblesModel(int id_cita, String medico, String fechahora) {
        this.id_cita = id_cita;
        this.medico = medico;
        this.fechahora = fechahora;
    }   
    
     public int getId_cita() {
        return id_cita;
     }
     
     public void setId_cita(int id_cita){
         this.id_cita = id_cita;
     }
     
     public String getMedico(){
         return medico;
     }
     
     public void setMedico(String medico){
         this.medico = medico;
     }
     public String getFechahora(){
         return fechahora;
     }
     
     public void setFechahora(String fechahora){
         this.fechahora = fechahora;
     }
     
}
