/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

public class BeneficiariosModel {
    private String id_beneficiario ;
    private String tipo_documento; 
    private String documento;
    private String nombre;
    private String apellido ;
    private String usuario ;
    private String contrasena ;
    private String telefono1 ;
    private String telefono2 ;
    private String email1 ;
    private String email2 ;
    private String estado ;
    private String direccion ;

    public String getId_beneficiario() {
        return id_beneficiario;
    }

    public void setId_beneficiario(String id_beneficiario) {
        this.id_beneficiario = id_beneficiario;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contraseña) {
        this.contrasena = contraseña;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public BeneficiariosModel() {
    }

    public BeneficiariosModel(String id_beneficiario, String tipo_documento, String documento, String nombre, String apellido, String usuario, String contraseña, String telefono1, String telefono2, String email1, String email2, String estado) {
        this.id_beneficiario = id_beneficiario;
        this.tipo_documento = tipo_documento;
        this.documento = documento;
        this.nombre = nombre;
        this.apellido = apellido;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.email1 = email1;
        this.email2 = email2;
        this.estado = estado;
        this.direccion="";
    }
    
}
