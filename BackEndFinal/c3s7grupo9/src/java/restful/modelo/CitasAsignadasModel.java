
package restful.modelo;




public class CitasAsignadasModel {
    private int id_cita;
    private String medico;
    private String fechahora;
    private String beneficiario;
//    private String diagnostico;
//    private String fechahoraatencion;
    
    public CitasAsignadasModel() {
    }
    
    public CitasAsignadasModel(int id_cita, String medico, String fechahora, String beneficiario) {
        this.id_cita = id_cita;
        this.medico = medico;
        this.fechahora = fechahora;
        this.beneficiario= beneficiario;
    }   
    
     public int getId_cita() {
        return id_cita;
     }
     
     public void setId_cita(int id_cita){
         this.id_cita = id_cita;
     }
     
     public String getMedico(){
         return medico;
     }
     
     public void setMedico(String medico){
         this.medico = medico;
     }
     public String getFechahora(){
         return fechahora;
     }
     
     public void setFechahora(String fechahora){
         this.fechahora = fechahora;
     }
     
     public String getBeneficiario (){
         return beneficiario;
     }
     
     public void setBeneficiario (String beneficiario){
         this.beneficiario=beneficiario;
     }
     
}

