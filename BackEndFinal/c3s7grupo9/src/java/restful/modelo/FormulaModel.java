/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

import java.util.ArrayList;

/**
 *
 * @author frendon
 */
public class FormulaModel {
    ArrayList<ItemFormulaModel> medicamento = new ArrayList<>();

    public ArrayList<ItemFormulaModel> getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(ArrayList<ItemFormulaModel> medicamento) {
        this.medicamento = medicamento;
    }
    
}
