/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

import java.util.ArrayList;

/**
 *
 * @author frendon
 */
public class Informe3Model extends Informe2Model {
    private String diagnostico;
    private FormulaModel formula;

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public FormulaModel getFormula() {
        return formula;
    }

    public void setFormula(FormulaModel formula) {
        this.formula = formula;
    }
}





