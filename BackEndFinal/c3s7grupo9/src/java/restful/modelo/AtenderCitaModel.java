/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

/**
 *
 * @author PC
 */
public class AtenderCitaModel {
    private int id_cita;
    private String diagnostico;
    private FormulaModel formula;   

    public int getId_cita() {
        return id_cita;
    }

    public void setId_cita(int id_cita) {
        this.id_cita = id_cita;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public FormulaModel getFormula() {
        return formula;
    }

    public void setFormula(FormulaModel formula) {
        this.formula = formula;
    }
    
}
