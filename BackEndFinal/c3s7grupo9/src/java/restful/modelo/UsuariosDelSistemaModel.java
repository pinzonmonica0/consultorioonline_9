
package restful.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement


public class UsuariosDelSistemaModel {
    private String Id_usuario;
    private String perfil;
    private String usuario;
    private String contrasena;
    private String tipo_de_documento;
    private String documento;
    private String nombres;
    private String apellidos;
    private String correo_electronico;
    private String estado;

        public UsuariosDelSistemaModel() {
        }

        public UsuariosDelSistemaModel(String Id_usuario, String perfil, String usuario, String contrasena, String tipo_de_documento, String documento, String nombre, String apellido, String correo_electronico, String estado) {
            this.Id_usuario = Id_usuario;
            this.perfil = perfil;
            this.usuario = usuario;
            this.contrasena = contrasena;
            this.tipo_de_documento = tipo_de_documento;
            this.documento = documento;
            this.nombres = nombre;
            this.apellidos = apellido;
            this.correo_electronico = correo_electronico;
            this.estado = estado;
        }

        public String getId_usuario() {
            return Id_usuario;
        }

        public void setId_usuario(String Id_usuario) {
            this.Id_usuario = Id_usuario;
        }

        public String getPerfil() {
            return perfil;
        }

        public void setPerfil(String perfil) {
            this.perfil = perfil;
        }

        public String getUsuario() {
            return usuario;
        }

        public void setUsuario(String usuario) {
            this.usuario = usuario;
        }

        public String getContrasena() {
            return contrasena;
        }

        public void setContraseña(String contraseña) {
            this.contrasena = contraseña;
        }

        public String getTipo_de_documento() {
            return tipo_de_documento;
        }

        public void setTipo_de_documento(String tipo_de_documento) {
            this.tipo_de_documento = tipo_de_documento;
        }

        public String getDocumento() {
            return documento;
        }

        public void setDocumento(String documento) {
            this.documento = documento;
        }

        public String getNombre() {
            return nombres;
        }

        public void setNombre(String nombre) {
            this.nombres = nombre;
        }

        public String getApellido() {
            return apellidos;
        }

        public void setApellido(String apellido) {
            this.apellidos = apellido;
        }

        public String getCorreo_electronico() {
            return correo_electronico;
        }

        public void setCorreo_electronico(String correo_electronico) {
            this.correo_electronico = correo_electronico;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }
    
    }

    


