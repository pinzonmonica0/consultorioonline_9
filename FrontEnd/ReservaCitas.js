
// ruta del Backend para el ejemplo Remota y local (el back debe estar corriendo)
const url = "https://minticloud.uis.edu.co/c3s7grupo9/ConsultorioOnLine/CitasDisponibles" 
//const url = "http://localhost:8080/Tutorial/api/clientes"

// definición de constantes de trabajo creadas a partir de elementos de la página web
// se puende ubicar de dos formas querySelector o getElementById

const contenedor = document.querySelector('tbody')
const bcerrar = document.getElementById('cerrar')
const bcerrarx = document.getElementById('cerrarx')
const modalCitas = new bootstrap.Modal(document.getElementById('modalCita'))
const formClientes = document.getElementById("frmPrincipal")
const idCita = document.getElementById('id')
const nombreMedico = document.getElementById('nombre')
const fechaCita = document.getElementById('fecha')
const usuarioBeneficiario = document.getElementById('usuario')
const claveBeneficiario = document.getElementById('clave')


const cuerpo = document.querySelector('body')

// definir variable de trabajo para el resultado de consumir el backend(resultados) y la acción
// a realizar cuando se presione el boton guardar del formulario que puede ser POST o PUT
var resultados = ''
var accion = ''


// agregar eventos a los objetos que lo requieran

// agregar evento click a los botones de cerrar del formulario, que puede ser la x de la parte superior
// y el boton cerrar de la parte inferior del formulario modal

bcerrar.addEventListener('click', () => {
    modalCitas.hide()
})
bcerrarx.addEventListener('click', () => {
    modalCitas.hide()
})


// agregar evento click a cualquier elemento de la página identificandolo por la clase asignada
document.addEventListener("click", (e) => {

        //  traer la información de la fila en la que esta ubicado el  boton editar
        // recordar que estos botones son cargados de forma dinámica de acuerdo con el contenido consumido del
        // backend
    if (e.target.matches(".btnEditar")) {
        // seleccionar la fila del boton pulsado
        const fila = e.target.parentNode.parentNode
        // extraer la información de cada una de las celdas para enviar a los input correspondiente
        idCita.value = fila.children[0].innerHTML
        nombreMedico.value = fila.children[1].innerHTML
        fechaCita.value = fila.children[2].innerHTML
        idCita.disabled = true
        nombreMedico.disabled = true
        fechaCita.disabled = true

        // defino el metodo a utilizar que para este caso es actualizar [ PUT ]
        accion = 'PUT'
        // mostrar el formulario modal
        modalCitas.show()
    }

})


// agregar evento [ submit ] al formulario que permite enviar la información al backend
// puede ser POST o puede ser PUT dependiendo de la acción selecionada previamente
formClientes.addEventListener('submit', (e) => {
    e.preventDefault()
    // se define el metodo por defecto del boton submit [ Guardar ]
    let metodo = "POST"

    // se cambia el metodo si el boton que ha llamado el formulario es el boton editar
    if (accion == 'PUT') {
        metodo = "PUT"
    }

    // se llama la constante ajax que enviará la información al backend de acuerdo al metodo seleccionado
    ajax({
        // url del backend
        url: url,
        // el metodo a utilizar que puede ser ( GET, DELETE, POST, PUT)
        method: metodo,
        // se definen los encabezados 
        headers: {
            'Content-Type': 'application/json'
        },
        // este metodo se ejecuta cuando la accion es correcta
        success: (res) => {
            if(metodo=="POST"){
             // Validar si se insertó o no el registro en el backend
                if(res.nombre != null){
                    alertify.success("Registro Insertado")
                }
                else {
                    alertify.error("No se pudo inserto")
                }

            }
            else{                              
                
                // Validar si se modificó o no el registro en el backend
                if(res.Accion=="Cita Reservada"){
                    //alert(res.Accion) 
                    alertify.success("Reserva realizada")
                }
                else {
                    alertify.error("No se pudo hacer a reserva")
                }

            }
            // una funcion promesa que detiene la ejecución del script unos milisegundos para este cargados
            // 1000 que corresponde a un segundo
            setTimeout(function(){
                location.reload();
            },1000);
            
        },
        // este metodo se ejecuta cuando la accion es incorrecta
        error: (err) =>
            $form.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`),
        // se envia la información en formato JSON hacia el backend [ La información varia de acuerdo con
        // el diseño de la entidad o tabla ]
        data: {
            "id_cita": idCita.value,
            "usuario": usuarioBeneficiario.value,
            "clave": claveBeneficiario.value,
            
        },
        
    });

    // se cierra la ventana modal
    modalCitas.hide()
})

// core programa que administra el consumo del backend

// definir la estructura que comunicará el frontEnd con el Backend a través del objeto XMLHttpRequest
const ajax = (options) => {
    // parametros del objeto
    let { url, method, success, error, data } = options;

    console.log(data)
    // se define el objeto que va a traer la información del backend
    const xhr = new XMLHttpRequest();

    // se agrega evento al objeto de tipo readystatechange que identifica un cambio en el encabezado al realizar
    // la petición al backend
    xhr.addEventListener("readystatechange", (e) => {
        //console.log(xhr.status);
        //console.log(xhr.responseText);

        if (xhr.readyState !== 4) return;
        // verifica el status de de respuesta del backend es decir se tiene una respuesta positiva
        // del backend
        if (xhr.status >= 200 && xhr.status < 300) {
            // convierte la respuesta del backend en una información en formato JSON
            let json = JSON.parse(xhr.responseText);
            success(json);
        } else  {
            // se genera un erro de estatus es decir la respuesta del backend esta fuera del rango
            // posibles status de respuesta https://developer.mozilla.org/es/docs/Web/HTTP/Status
            resultados = `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <h1>Página no encontrada</h1>
            </body>
            </html>`
               cuerpo.innerHTML = resultados
        }
    });

    // por defecto define el metodo GET
    xhr.open(method || "GET", url);
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.send(JSON.stringify(data));
};


// se define el objeto que trae la información del backend a través del método GET

const getCliente = () => {

    ajax({
        url: url,
        method: "GET",
        success: (rs) => {
            console.log(rs);
            // recorrer el objeto JSON generado por el backend y constrir el código HMTL para insertar información
            // en el cuerpo de la tabla
            rs.forEach((citas) => {
                resultados += `<tr>
                    <td width="10%" class='btnFila'>${citas.id_cita}</td>
                    <td width="25%" class='btnFila'>${citas.medico}</td>
                    <td width="25%" class='btnFila'>${citas.fechahora}</td>

                    <td class="text-center" width="20%">            
                    <i class="btnEditar fa fa-pencil btn btn-primary"></i>     
                        
                    
                    </td>
                </tr>`
            });
            // Inserta la información construida en el cuerpo de la tabla
            contenedor.innerHTML = resultados

            // Agregar la funcionadlidad [ DataTable ] a la tabla que presneta resultados
            $(document).ready(function () {
                $('#Citas').DataTable({
                    // se traduce la estructura del objeto DataTable
                    language: {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    },
                });
            });
            resultados = ''
        },
        error: (err) => {
            console.log(err);
            $table.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`);
        },
    });
};

// se llama el mento que trae la información del backend por medio del metodo GET
document.addEventListener("DOMContentLoaded", getCliente(""));

// define estilos de diseño para el objeto alertify
alertify.defaults.transition = "slide";
alertify.defaults.theme.ok = "btn btn-primary";
alertify.defaults.theme.cancel = "btn btn-danger";
alertify.defaults.theme.input = "form-control";
