
// ruta del Backend para el ejemplo Remota y local (el back debe estar corriendo)
const url = "https://minticloud.uis.edu.co/c3s7grupo9/ConsultorioOnLine/Informe3" 
//const url = "http://localhost:8080/Tutorial/api/clientes"

// definición de constantes de trabajo creadas a partir de elementos de la página web
// se puende ubicar de dos formas querySelector o getElementById

const contenedor = document.querySelector('tbody')
const bcerrar = document.getElementById('cerrar')
const bcerrarx = document.getElementById('cerrarx')
//const modalCitas = new bootstrap.Modal(document.getElementById('modalCita'))
const formClientes = document.getElementById("frmPrincipal")
//const idCita = document.getElementById('id')
//onst nombreMedico = document.getElementById('nombre')
//const fechaCita = document.getElementById('fecha')
//const usuarioBeneficiario = document.getElementById('usuario')
//const claveBeneficiario = document.getElementById('clave')


const cuerpo = document.querySelector('body')

// definir variable de trabajo para el resultado de consumir el backend(resultados) y la acción
// a realizar cuando se presione el boton guardar del formulario que puede ser POST o PUT
var resultados = ''
var accion = ''


// agregar eventos a los objetos que lo requieran

// agregar evento click a los botones de cerrar del formulario, que puede ser la x de la parte superior
// y el boton cerrar de la parte inferior del formulario modal

//bcerrar.addEventListener('click', () => {
//    modalCitas.hide()
//})
//bcerrarx.addEventListener('click', () => {
//    modalCitas.hide()
//})


// agregar evento [ submit ] al formulario que permite enviar la información al backend
// puede ser POST o puede ser PUT dependiendo de la acción selecionada previamente


// core programa que administra el consumo del backend

// definir la estructura que comunicará el frontEnd con el Backend a través del objeto XMLHttpRequest
const ajax = (options) => {
    // parametros del objeto
    let { url, method, success, error, data } = options;

    console.log(data)
    // se define el objeto que va a traer la información del backend
    const xhr = new XMLHttpRequest();

    // se agrega evento al objeto de tipo readystatechange que identifica un cambio en el encabezado al realizar
    // la petición al backend
    xhr.addEventListener("readystatechange", (e) => {
        //console.log(xhr.status);
        //console.log(xhr.responseText);

        if (xhr.readyState !== 4) return;
        // verifica el status de de respuesta del backend es decir se tiene una respuesta positiva
        // del backend
        if (xhr.status >= 200 && xhr.status < 300) {
            // convierte la respuesta del backend en una información en formato JSON
            let json = JSON.parse(xhr.responseText);
            success(json);
        } else  {
            // se genera un erro de estatus es decir la respuesta del backend esta fuera del rango
            // posibles status de respuesta https://developer.mozilla.org/es/docs/Web/HTTP/Status
            resultados = `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <h1>Página no encontrada</h1>
            </body>
            </html>`
               cuerpo.innerHTML = resultados
        }
    });

    // por defecto define el metodo GET
    xhr.open(method || "GET", url);
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.send(JSON.stringify(data));
};


// se define el objeto que trae la información del backend a través del método GET

const getCliente = () => {

    ajax({
        url: url,
        method: "GET",
        success: (rs) => {
            console.log(rs);
            // recorrer el objeto JSON generado por el backend y constrir el código HMTL para insertar información
            // en el cuerpo de la tabla
            rs.forEach((citas) => {
                resultados += `<tr>
                    <td width="10%" class='btnFila'>${citas.documento_beneficiario}</td>
                    <td width="25%" class='btnFila'>${citas.nombre_beneficiario}</td>
                    <td width="25%" class='btnFila'>${citas.telefono}</td>
                    <td width="25%" class='btnFila'>${citas.nombre_medico}</td>
                    <td width="25%" class='btnFila'>${citas.fecha_cita}</td>
                    <td width="25%" class='btnFila'>${citas.diagnostico}</td>                  

                </tr>`
            });
            // Inserta la información construida en el cuerpo de la tabla
            contenedor.innerHTML = resultados

            // Agregar la funcionadlidad [ DataTable ] a la tabla que presneta resultados
            $(document).ready(function () {
                $('#Citas').DataTable({
                    // se traduce la estructura del objeto DataTable
                    language: {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    },
                });
            });
            resultados = ''
        },
        error: (err) => {
            console.log(err);
            $table.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`);
        },
    });
};


// se llama el mento que trae la información del backend por medio del metodo GET
document.addEventListener("DOMContentLoaded", getCliente(""));

// define estilos de diseño para el objeto alertify
alertify.defaults.transition = "slide";
alertify.defaults.theme.ok = "btn btn-primary";
alertify.defaults.theme.cancel = "btn btn-danger";
alertify.defaults.theme.input = "form-control";
