
// ruta del Backend para el ejemplo Remota y local (el back debe estar corriendo)
const url = "https://minticloud.uis.edu.co/c3s7grupo9/ConsultorioOnLine/Beneficiarios"
//const url = "http://localhost:8080/Tutorial/api/clientes"

// definición de constantes de trabajo creadas a partir de elementos de la página web
// se puende ubicar de dos formas querySelector o getElementById

const contenedor = document.querySelector('tbody')
const bcerrar = document.getElementById('cerrar')
const bcerrarx = document.getElementById('cerrarx')
const modalClientes = new bootstrap.Modal(document.getElementById('modalCliente'))
const formClientes = document.getElementById("frmPrincipal")
const idCliente = document.getElementById('idBeneficiario')
const tipoDocumento = document.getElementById('tipoDocumento')
const documento = document.getElementById('documento')
const nombre = document.getElementById('nombre')
const apellido = document.getElementById('apellido')
const usuario = document.getElementById('usuario')
const contrasena = document.getElementById('contrasena')
const telefono1 = document.getElementById('telefono1')
const telefono2 = document.getElementById('telefono2')
const email1 = document.getElementById('email1')
const email2 = document.getElementById('email2')
const estado = document.getElementById('estado')

const cuerpo = document.querySelector('body')

// definir variable de trabajo para el resultado de consumir el backend(resultados) y la acción
// a realizar cuando se presione el boton guardar del formulario que puede ser POST o PUT
var resultados = ''
var accion = ''


// agregar eventos a los objetos que lo requieran

// evento click del boton crear para que muestre el formulario modal, limpie el contenido
// de las cajas de texto y habilite la caja de texto del id del formulario (puede variar de 
// acuerdo al diseño definido), además, define la accion a realizar que para este caso es crear
btnCrear.addEventListener('click', () => {

    idCliente.value = ''
    tipoDocumento.value = ''
    documento.value = ''
    nombre.value = ''
    apellido.value = ''
    usuario.value = ''
    contrasena.value = ''
    telefono1.value=''
    telefono1.value=''
    email1.value=''
    email2.value=''
    estado.value=''
    //idCliente.disabled = false
    modalClientes.show()
    accion = 'POST'
})

// agregar evento click a los botones de cerrar del formulario, que puede ser la x de la parte superior
// y el boton cerrar de la parte inferior del formulario modal

bcerrar.addEventListener('click', () => {
    modalClientes.hide()
})
bcerrarx.addEventListener('click', () => {
    modalClientes.hide()
})


// agregar evento click a cualquier elemento de la página identificandolo por la clase asignada
document.addEventListener("click", (e) => {

    // si la clase asignada es un btnBorrar se asigna la funcionalidad al boton borrar
    if (e.target.matches(".btnBorrar")) {

        //  traer la información de la fila en la que esta ubicado el  boton cerrar
        // recordar que estos botones son cargados de forma dinámica de acuerdo con el contenido consumido del
        // backend
        const fila = e.target.parentNode.parentNode
        // para este ejemplo: traer la información de la segunda celda que contine el nombre del cliente
        // la información puede variar de acuedo con el diseño definido
        const nombre = fila.children[3].innerHTML
        // traer el id del la fila seleccionada
        const id = fila.children[0].innerHTML
        console.log(id)
        // se muestra un cuadro de dialogo de confirmación para eliminar o no el registro( se utiliza la funcionalidad
        // de alertify) si se pulsa el boton ok se elimina el registro de lo contario no se realiza ninguna acción
        alertify.confirm('Eliminar Beneficiario', `¿Estás seguro de eliminar el beneficiario : ${nombre}?`,
            function () {
                // si el boton ok es presionado se llama el metodo ajax (construido más adelante)
                // se indica el metodo a utilizar  [ DELETE ]
                // se envian encabezados para indicar que la información será enviada en formato JSON
                ajax({
                    url: url + "/" + id,
                    method: "DELETE",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    // si se ejecuta la acción de forma correcta se recarga la  página
                    success: (res) =>  {
                        //alert(res.Accion)
                        // validando desde el backend si se eliminó o on el registro (de acuerdo al número de filas afectas)
                        if(res.Accion=="Registro borrado"){
                            alertify.success( 'Registro eliminado')
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }
                        else{
                            alertify.error('No se pudo eliminar el registro')
                        }

                    },
                    // si se presenta un error en el llamado del api se presenta un mensaje de error
                    error: (err) => alert(err),
                });
                
            },
            // es el rechazo de la accion de borrar
            function () {
                alertify.error('Se cancelo la acción')
            });


    }


        //  traer la información de la fila en la que esta ubicado el  boton editar
        // recordar que estos botones son cargados de forma dinámica de acuerdo con el contenido consumido del
        // backend
    if (e.target.matches(".btnEditar")) {
        // seleccionar la fila del boton pulsado
        const fila = e.target.parentNode.parentNode
        // extraer la información de cada una de las celdas para enviar a los input correspondiente
        //alert(fila.children[0].innerHTML)
        idCliente.value = fila.children[0].innerHTML
        tipoDocumento.value = fila.children[1].innerHTML
        documento.value = fila.children[2].innerHTML
        nombre.value = fila.children[3].innerHTML
        apellido.value = fila.children[4].innerHTML
        usuario.value = fila.children[5].innerHTML
        contrasena.value = fila.children[6].innerHTML
        telefono1.value = fila.children[7].innerHTML
        telefono2.value = fila.children[8].innerHTML
        email1.value = fila.children[9].innerHTML
        email2.value = fila.children[10].innerHTML
        estado.value = fila.children[11].innerHTML
        idCliente.disabled = true

        // defino el metodo a utilizar que para este caso es actualizar [ PUT ]
        accion = 'PUT'
        // mostrar el formulario modal
        modalClientes.show()
    }

})


// agregar evento [ submit ] al formulario que permite enviar la información al backend
// puede ser POST o puede ser PUT dependiendo de la acción selecionada previamente
formClientes.addEventListener('submit', (e) => {
    e.preventDefault()
    // se define el metodo por defecto del boton submit [ Guardar ]
    let metodo = "POST"

    // se cambia el metodo si el boton que ha llamado el formulario es el boton editar
    if (accion == 'PUT') {
        metodo = "PUT"
    }

    // se llama la constante ajax que enviará la información al backend de acuerdo al metodo seleccionado
    ajax({
        // url del backend
        url: url,
        // el metodo a utilizar que puede ser ( GET, DELETE, POST, PUT)
        method: metodo,
        // se definen los encabezados 
        headers: {
            'Content-Type': 'application/json'
        },
        // este metodo se ejecuta cuando la accion es correcta
        success: (res) => {
            if(metodo=="POST"){
             // Validar si se insertó o no el registro en el backend
                if(res.nombre != null){
                    alertify.success("Registro Creado")
                }
                else {
                    alertify.error("No se pudo crear elregistro")
                }

            }
            else{
                 
                // Validar si se modificó o no el registro en el backend
                if(res.nombre!=""){
                    alertify.success("Registro Actualizado")
                }
                else {
                    alertify.error("No se pudo actualizr")
                }

            }
            // una funcion promesa que detiene la ejecución del script unos milisegundos para este cargados
            // 1000 que corresponde a un segundo
            setTimeout(function(){
                location.reload();
            },1000);
            
        },
        // este metodo se ejecuta cuando la accion es incorrecta
        error: (err) =>
            $form.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`),
        // se envia la información en formato JSON hacia el backend [ La información varia de acuerdo con
        // el diseño de la entidad o tabla ]
        data: {
            "id_beneficiario":idCliente.value,
            "tipo_documento": tipoDocumento.value,
            "documento": documento.value,
            "nombre": nombre.value,
            "apellido": apellido.value,
            "usuario": usuario.value,
            "contrasena": contrasena.value,
            "telefono1": telefono1.value,
            "telefono2": telefono2.value,
            "email1": email1.value,
            "email2": email2.value,
            "estado": estado.value,
            "direccion": "",
        },
        
    });

    // se cierra la ventana modal
    modalClientes.hide()
})

// core programa que administra el consumo del backend

// definir la estructura que comunicará el frontEnd con el Backend a través del objeto XMLHttpRequest
const ajax = (options) => {
    // parametros del objeto
    
    let { url, method, success, error, data } = options;
    // se define el objeto que va a traer la información del backend
    //alert(data)
    const xhr = new XMLHttpRequest();

    // se agrega evento al objeto de tipo readystatechange que identifica un cambio en el encabezado al realizar
    // la petición al backend
    xhr.addEventListener("readystatechange", (e) => {
        //alert(xhr.status)
        if (xhr.readyState !== 4) return;
        // verifica el status de de respuesta del backend es decir se tiene una respuesta positiva
        // del backend
        if (xhr.status >= 200 && xhr.status < 300) {
            // convierte la respuesta del backend en una información en formato JSON
            //alert(xhr.responseText)
            let json = JSON.parse(xhr.responseText);
            success(json);
        } else  {
            // se genera un erro de estatus es decir la respuesta del backend esta fuera del rango
            // posibles status de respuesta https://developer.mozilla.org/es/docs/Web/HTTP/Status
            resultados = `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <h1>Página no encontrada</h1>
            </body>
            </html>`
               cuerpo.innerHTML = resultados
        }
    });

    // por defecto define el metodo GET
    xhr.open(method || "GET", url);
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.send(JSON.stringify(data));
};


// se define el objeto que trae la información del backend a través del método GET

const getCliente = () => {

    ajax({
        url: url,
        method: "GET",
        success: (rs) => {
            console.log(rs);
            // recorrer el objeto JSON generado por el backend y constrir el código HMTL para insertar información
            // en el cuerpo de la tabla
            rs.forEach((Beneficiarios) => {
                resultados += `<tr>
                    <td width="15%" class='btnFila'>${Beneficiarios.id_beneficiario}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.tipo_documento}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.documento}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.nombre}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.apellido}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.usuario}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.contrasena}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.telefono1}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.telefono2}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.email1}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.email2}</td>
                    <td width="15%" class='btnFila'>${Beneficiarios.estado}</td>
                    <td class="text-center" width="20%">            
                    <i class="btnEditar fa fa-pencil btn btn-primary"></i>     
                        
                    <i class="btnBorrar fa fa-trash btn btn-danger"></i>
                    </td>
                </tr>`
            });
            // Inserta la información construida en el cuerpo de la tabla
            contenedor.innerHTML = resultados

            // Agregar la funcionadlidad [ DataTable ] a la tabla que presneta resultados
            $(document).ready(function () {
                $('#clientes').DataTable({
                    // se traduce la estructura del objeto DataTable
                    language: {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    },
                });
            });
            resultados = ''
        },
        error: (err) => {
            console.log(err);
            $table.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`);
        },
    });
};

// se llama el mento que trae la información del backend por medio del metodo GET
document.addEventListener("DOMContentLoaded", getCliente(""));

// define estilos de diseño para el objeto alertify
alertify.defaults.transition = "slide";
alertify.defaults.theme.ok = "btn btn-primary";
alertify.defaults.theme.cancel = "btn btn-danger";
alertify.defaults.theme.input = "form-control";
