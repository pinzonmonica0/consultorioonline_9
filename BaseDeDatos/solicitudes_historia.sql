USE consultorioonline_9;
CREATE TABLE Solicitudes_Historia(
	beneficiario VARCHAR(15) NOT NULL,
	fechaHora DATETIME NOT NULL,
	usuario VARCHAR(15) NULL,
	estado VARCHAR(10) NOT NULL,
	fechaCambioEstado DATETIME NULL,
	CONSTRAINT Solicitudes_Historia_pk PRIMARY KEY(beneficiario,fechaHora),
	CONSTRAINT Solicitudes_Historia_estado_ck CHECK(estado='EnCurso' OR estado='Aprobada' OR estado='Rechazada'),
	CONSTRAINT FK_Solicitudes_Historia_usuario FOREIGN KEY(usuario) REFERENCES usuarios_del_sistema(Id_usuario),
	CONSTRAINT FK_Solicitudes_Historia_beneficiario FOREIGN KEY(beneficiario) REFERENCES beneficiarios(id_beneficiario)
);