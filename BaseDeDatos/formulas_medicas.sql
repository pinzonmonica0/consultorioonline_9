CREATE TABLE Formulas_Medicas(
IdCita INT NOT NULL,
Id_medicamento VARCHAR (20) NOT NULL,
Cantidad DOUBLE NOT NULL,
Instrucciones VARCHAR (300) NOT NULL,
CONSTRAINT Formulas_Medicas_pk PRIMARY KEY(IdCita,Id_medicamento),
CONSTRAINT FK_Formulas_Medicas_cita FOREIGN KEY(IdCita) REFERENCES Agenda_citas(IdCita),
CONSTRAINT FK_Formulas_Medicas_medicamento FOREIGN KEY(Id_medicamento) REFERENCES medicamentos(Codigo_medicamento));






