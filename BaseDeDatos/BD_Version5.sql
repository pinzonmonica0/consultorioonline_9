USE c3s7grupo9;

CREATE TABLE tipos_de_documento (
  Id_tipo_de_documento VARCHAR(3) NOT NULL,
  tipo_de_documento VARCHAR(40) NOT NULL,
  CONSTRAINT tipos_de_documento_pk PRIMARY KEY(Id_tipo_de_documento));

CREATE TABLE opciones_sistema (
	Codigo VARCHAR (25) NOT NULL,
	Descripcion VARCHAR (80) NOT NULL,
	Pagina VARCHAR (25) NULL,
	Destino VARCHAR (60) NOT NULL,
	NombreWeb VARCHAR (255) NOT NULL,
	CONSTRAINT PK_opciones_sistema PRIMARY KEY(Codigo)); 

CREATE TABLE perfiles_usuario(
idPerfil VARCHAR(25) PRIMARY KEY,
descripcion VARCHAR (30) NOT NULL);


CREATE TABLE opciones_perfil (
	Perfil VARCHAR (25) NOT NULL,
	Opcion VARCHAR (25) NOT NULL,
	CONSTRAINT PK_opciones_perfil PRIMARY KEY(Perfil,Opcion),
	CONSTRAINT FK_opciones_perfil_opc FOREIGN KEY(Opcion) REFERENCES opciones_sistema(Codigo),
	CONSTRAINT FK_opciones_perfil_perf FOREIGN KEY(Perfil) REFERENCES perfiles_usuario(idPerfil)); 

 CREATE TABLE medicamentos (
	Codigo_medicamento VARCHAR(15) PRIMARY KEY,
	Nombre_generico VARCHAR(100) NOT NULL,
	Nombre_Comercial VARCHAR(100) NOT NULL,
	Laboratorio VARCHAR(100) NOT NULL,
	Estado VARCHAR(10) NOT NULL,
	Presentacion VARCHAR(50) NULL,
	CONSTRAINT medicamentos_estado_ck CHECK(Estado='Activo' OR Estado='Inactivo'));
	
CREATE TABLE beneficiarios(
	id_beneficiario VARCHAR(15) NOT NULL PRIMARY KEY,
	tipo_documento VARCHAR(3)NOT NULL,
	documento VARCHAR(30)NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	apellido VARCHAR(50) NOT NULL,
	usuario VARCHAR(50) NOT NULL,
	contrasena VARCHAR (23) NOT NULL,
	telefono1 VARCHAR (20) NOT NULL,
	telefono2 VARCHAR (20) NULL,
	email1 VARCHAR(255) NOT NULL,
	email2 VARCHAR(255) NULL,
	direccion VARCHAR(100) NULL,
	estado VARCHAR(10) NOT NULL,
	CONSTRAINT beneficiarios_usuario_ck UNIQUE(usuario),
	CONSTRAINT beneficiarios_estado_ck CHECK(estado='Activo' OR estado='Inactivo'),
	CONSTRAINT FK_beneficiarios_tipo_documento FOREIGN KEY(tipo_documento) REFERENCES tipos_de_documento(Id_tipo_de_documento));
	
CREATE TABLE usuarios_del_sistema(
	Id_usuario VARCHAR(15) NOT NULL PRIMARY KEY,
	perfil VARCHAR(25) NOT NULL,
	usuario VARCHAR(50) NOT NULL,
	contrasena VARCHAR(23) DEFAULT NULL,
	tipo_de_documento VARCHAR(3) NOT NULL,
	documento VARCHAR(30) NOT NULL,
	nombres VARCHAR(100) NOT NULL,
	apellidos VARCHAR(100) NOT NULL,
	correo_electronico VARCHAR(255) NOT NULL,
	estado VARCHAR (10) NOT NULL,
	CONSTRAINT usuarios_del_sistema_usuario_ck UNIQUE(usuario),
	CONSTRAINT usuarios_del_sistema_estado_ck CHECK(estado='Activo' OR estado='Inactivo'),
	CONSTRAINT FK_usuarios_del_sistema_tipo_documento FOREIGN KEY(tipo_de_documento) REFERENCES tipos_de_documento(Id_tipo_de_documento),
	CONSTRAINT FK_usuarios_del_sistema_perfil FOREIGN KEY(perfil) REFERENCES perfiles_usuario(idPerfil));
	
CREATE TABLE Agenda_citas(
	IdCita INT AUTO_INCREMENT PRIMARY KEY,
	Medico VARCHAR(15) NOT NULL,
	FechaHora DATETIME NOT NULL,
	Beneficiario VARCHAR(15) NULL,
	Diagnostico VARCHAR (700) NULL,
	FechaHoraAtencion DATETIME NULL,
	CONSTRAINT FK_Agenda_citas_medico FOREIGN KEY(Medico) REFERENCES usuarios_del_sistema(Id_usuario),
	CONSTRAINT FK_Agenda_citas_beneficiario FOREIGN KEY(Beneficiario) REFERENCES beneficiarios(id_beneficiario));
	
CREATE TABLE Formulas_Medicas(
	IdCita INT NOT NULL,
	Id_medicamento VARCHAR (20) NOT NULL,
	Cantidad DOUBLE NOT NULL,
	Instrucciones VARCHAR (300) NOT NULL,
	CONSTRAINT Formulas_Medicas_pk PRIMARY KEY(IdCita,Id_medicamento),
	CONSTRAINT FK_Formulas_Medicas_cita FOREIGN KEY(IdCita) REFERENCES Agenda_citas(IdCita),
	CONSTRAINT FK_Formulas_Medicas_medicamento FOREIGN KEY(Id_medicamento) REFERENCES medicamentos(Codigo_medicamento));
	

CREATE TABLE Solicitudes_Historia(
	beneficiario VARCHAR(15) NOT NULL,
	fechaHora DATETIME NOT NULL,
	usuario VARCHAR(15) NULL,
	estado VARCHAR(10) NOT NULL,
	fechaCambioEstado DATETIME NULL,
	CONSTRAINT Solicitudes_Historia_pk PRIMARY KEY(beneficiario,fechaHora),
	CONSTRAINT Solicitudes_Historia_estado_ck CHECK(estado='EnCurso' OR estado='Aprobada' OR estado='Rechazada'),
	CONSTRAINT FK_Solicitudes_Historia_usuario FOREIGN KEY(usuario) REFERENCES usuarios_del_sistema(Id_usuario),
	CONSTRAINT FK_Solicitudes_Historia_beneficiario FOREIGN KEY(beneficiario) REFERENCES beneficiarios(id_beneficiario)
);	