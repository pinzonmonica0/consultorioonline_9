CREATE TABLE Agenda_citas(
IdCita INT AUTO_INCREMENT PRIMARY KEY,
Medico VARCHAR(15) NOT NULL,
FechaHora DATETIME NOT NULL,
Beneficiario VARCHAR(15) NULL,
Diagnostico VARCHAR (700) NULL,
FechaHoraAtencion DATETIME NULL,
CONSTRAINT FK_Agenda_citas_medico FOREIGN KEY(Medico) REFERENCES usuarios_del_sistema(Id_usuario),
CONSTRAINT FK_Agenda_citas_beneficiario FOREIGN KEY(Beneficiario) REFERENCES beneficiarios(id_beneficiario));
