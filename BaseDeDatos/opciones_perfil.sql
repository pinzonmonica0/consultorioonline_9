CREATE TABLE opciones_perfil (
	Perfil VARCHAR (25) NOT NULL,
	Opcion VARCHAR (25) NOT NULL,
	CONSTRAINT PK_opciones_perfil PRIMARY KEY(Perfil,Opcion),
	CONSTRAINT FK_opciones_perfil_opc FOREIGN KEY(Opcion) REFERENCES opciones_sistema(Codigo),
	CONSTRAINT FK_opciones_perfil_perf FOREIGN KEY(Perfil) REFERENCES perfiles_usuario(idPerfil)); 