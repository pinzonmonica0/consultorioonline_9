CREATE TABLE medicamentos (
	Codigo_medicamento VARCHAR(15) PRIMARY KEY,
	Nombre_generico VARCHAR(100) NOT NULL,
	Nombre_Comercial VARCHAR(100) NOT NULL,
	Laboratorio VARCHAR(100) NOT NULL,
	Estado VARCHAR(10) NOT NULL,
	Presentacion VARCHAR(50) NULL,
	CONSTRAINT medicamentos_estado_ck CHECK(Estado='Activo' OR Estado='Inactivo'));