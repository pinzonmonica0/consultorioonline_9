CREATE TABLE opciones_sistema (
	Codigo VARCHAR (25) NOT NULL,
	Descripcion VARCHAR (80) NOT NULL,
	Pagina VARCHAR (25) NULL,
	Destino VARCHAR (60) NOT NULL,
	NombreWeb VARCHAR (255) NOT NULL,
	CONSTRAINT PK_opciones_sistema PRIMARY KEY(Codigo)); 
