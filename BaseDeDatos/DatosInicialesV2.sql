USE c3s7grupo9;

INSERT INTO tipos_de_documento VALUES('CC', 'Cédula de Ciudadania');
INSERT INTO tipos_de_documento VALUES('TI', 'Tarjeta de Identidad');
INSERT INTO tipos_de_documento VALUES('PP', 'Pasaporte');
INSERT INTO tipos_de_documento VALUES('CE', 'Cédula de Extranjeria');
INSERT INTO tipos_de_documento VALUES('PEP', 'Permiso Especial de Permanencia');

INSERT INTO perfiles_usuario VALUES('SysAdmin', 'Administrador del Sistema');
INSERT INTO perfiles_usuario VALUES('Medico', 'Personal Medico');
INSERT INTO perfiles_usuario VALUES('Admin', 'Usuario Administrativo');

INSERT INTO opciones_sistema VALUES('00010001', 'Mantenimiento de Perfiles', 'Por definir', 'Usuario','Perfiles');
INSERT INTO opciones_sistema VALUES('00010002', 'Crear Opciones del Sistema', 'Por definir', 'Usuario','Opciones Sistema');
INSERT INTO opciones_sistema VALUES('00010003', 'Administración de Usuarios', 'Por definir', 'Usuario','Usuarios Sistema');
INSERT INTO opciones_sistema VALUES('00020001', 'Administración de Medicamentos', 'Por definir', 'Usuario','Medicamentos');
INSERT INTO opciones_sistema VALUES('00020002', 'Gestión Solicitudes Historias Clinicas', 'Por definir', 'Usuario','Gestión Historias');
INSERT INTO opciones_sistema VALUES('00020003', 'Gestión Agenda Citas', 'Por definir', 'Usuario','Gestión Agenda de Citas');
INSERT INTO opciones_sistema VALUES('00020004', 'Consulta Medica', 'Por definir', 'Usuario','Atención de Citas');
INSERT INTO opciones_sistema VALUES('00020005', 'Informes', 'Por definir', 'Usuario','Informes');
INSERT INTO opciones_sistema VALUES('00020006', 'Registro Beneficiarios', 'Por definir', 'Usuario','Beneficiarios');
INSERT INTO opciones_sistema VALUES('00040001', 'Reservar Citas', 'Por definir', 'Beneficiario','Reservar Citas');
INSERT INTO opciones_sistema VALUES('00040002', 'Consultar/Cancelar Citas', 'Por definir', 'Beneficiario','Citas Programadas');
INSERT INTO opciones_sistema VALUES('00040003', 'Solicictar Historia Clinica', 'Por definir', 'Beneficiario','Solicitud Historia');

INSERT INTO usuarios_del_sistema VALUES('A000000001','SysAdmin','admin','....','CC','1','Fulanito', 'Pascuales', 'fulanito@gmail.com','Activo');
INSERT INTO usuarios_del_sistema VALUES('M000000001','Medico','med1','....','CC','1','Pedro', 'Ruiz', 'pruiz@gmail.com','Activo');
INSERT INTO usuarios_del_sistema VALUES('M000000002','Medico','med2','....','CC','2','Angel', 'Rendon', 'angel@gmail.com','Activo');
INSERT INTO usuarios_del_sistema VALUES('M000000003','Medico','med3','....','CC','3','Juana', 'Alvarez', 'jAlv@gmail.com','Activo');

INSERT INTO beneficiarios VALUES('CC9800575','CC','98000575','Fredy','Rendon','alx976','....', '3182434826', '','alx976gmail.com','','Calle 1','Activo');
INSERT INTO beneficiarios VALUES('CC9800576','CC','98000576','Alex','Munoz','fren976','....', '3182434827', '','alx977gmail.com','','Calle Fals 123','Activo');

INSERT INTO medicamentos VALUES('0001','Acetaminofen','Dolex','Mk','Activo','Caja 20 Pastillas');
INSERT INTO medicamentos VALUES('0002','Ibuprofeno','Ibu','Genfar','Activo','Caja 16 Pastillas');
INSERT INTO medicamentos VALUES('0003','Jarabex','Jarabe X','X','Activo','Frasco de 250ml');

INSERT INTO opciones_perfil(Perfil,Opcion)
SELECT perfiles_usuario.idPerfil,opciones_sistema.codigo
FROM perfiles_usuario,opciones_sistema
WHERE perfiles_usuario.idPerfil='SysAdmin'








