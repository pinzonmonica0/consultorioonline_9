USE consultorioonline_9;

CREATE TABLE beneficiarios(
id_beneficiario INT AUTO_INCREMENT PRIMARY KEY,
tipo_documento VARCHAR(3)NOT NULL,
documento VARCHAR(30)NOT NULL,
nombre VARCHAR(50) NOT NULL,
apellido VARCHAR(50) NOT NULL,
usuario VARCHAR(50) NOT NULL,
contraseña VARCHAR (23) NOT NULL,
telefono1 VARCHAR (10) NOT NULL,
telefono2 VARCHAR (10) NULL,
email1 VARCHAR(10) NOT NULL,
email2 VARCHAR(10) NULL,
estado VARCHAR(10) NOT NULL);