USE consultorioonline_9;

CREATE TABLE perfiles_usuario(
idPerfil VARCHAR(25) PRIMARY KEY,
descripcion VARCHAR (30) NOT NULL,
opSistema VARCHAR(10) NOT NULL
);