/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author frendon
 */
@XmlRootElement
public class OpcionPerfilModel {
    private String Perfil;
    private String Opcion;

    public String getPerfil() {
        return Perfil;
    }

    public void setPerfil(String Perfil) {
        this.Perfil = Perfil;
    }

    public String getOpcion() {
        return Opcion;
    }

    public void setOpcion(String Opcion) {
        this.Opcion = Opcion;
    }
        
    public OpcionPerfilModel() {
    }
    
}
