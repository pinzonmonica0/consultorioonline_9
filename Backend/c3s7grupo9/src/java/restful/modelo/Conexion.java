package restful.modelo;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    private Connection con;
    private  String servidor ="local";
    private  String DB_DRIVER = "com.mysql.jdbc.Driver";
    private  String HOST = "localhost:3306";
    private  String DB = "c3s7grupo9";
    private  String URL = "jdbc:mysql://" + HOST + "/" + DB;
    private  String USERNAME = "root";
    private  String PASSWORD = "";
     public Conexion() {
         
         if (servidor.equals("remoto")){
             DB= "c3s7grupo9";
             USERNAME = "c3s7grupo9";
             PASSWORD = "HScg5AgC";
         }
         
         try {
             Class.forName(DB_DRIVER);
             con = DriverManager.getConnection(URL,USERNAME,PASSWORD);
             System.out.println("Conexión Exitosa");
         } catch (Exception e) {
             System.out.println("e = " + e.getMessage());
         }
    }
    
    public Connection getCon(){
        return con;
    }
    
    public void desconectar(){
        try {
            if(con!=null){
                con.close();
                System.out.println("conexion cerrada");
            }
        } catch (Exception e) {
            System.out.println("Error al desconecatr la BD");
        }
    }
}
