/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author frendon
 */
@XmlRootElement
public class OpcionSistemaModel {
    private String Codigo;
    private String Descripcion;
    private String Pagina;
    private String Destino;
    private String NombreWeb;
    
    public OpcionSistemaModel() {
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getPagina() {
        return Pagina;
    }

    public void setPagina(String Pagina) {
        this.Pagina = Pagina;
    }

    public String getDestino() {
        return Destino;
    }

    public void setDestino(String Destino) {
        this.Destino = Destino;
    }

    public String getNombreWeb() {
        return NombreWeb;
    }

    public void setNombreWeb(String NombreWeb) {
        this.NombreWeb = NombreWeb;
    }
    
    
   
}
