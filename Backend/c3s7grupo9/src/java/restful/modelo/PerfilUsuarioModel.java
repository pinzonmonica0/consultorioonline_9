package restful.modelo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author frendon
 */
@XmlRootElement
public class PerfilUsuarioModel {
    private String idPerfil;
    private String descripcion;
    
    public PerfilUsuarioModel() {
    }

    
    public String getIdPerfil() {
        return idPerfil;
    }
    
    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
        
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
