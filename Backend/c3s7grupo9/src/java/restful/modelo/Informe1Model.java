/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.modelo;

/**
 *
 * @author frendon
 */
public class Informe1Model {
    
    private String documento_beneficiario;
    private String nombre_beneficiario;
    private String telefono;
    private String direccion;

    public String getDocumento_beneficiario() {
        return documento_beneficiario;
    }

    public void setDocumento_beneficiario(String documento_beneficiario) {
        this.documento_beneficiario = documento_beneficiario;
    }

    public String getNombre_beneficiario() {
        return nombre_beneficiario;
    }

    public void setNombre_beneficiario(String nombre_beneficiario) {
        this.nombre_beneficiario = nombre_beneficiario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
}
