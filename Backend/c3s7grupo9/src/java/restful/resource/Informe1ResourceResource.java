/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import restful.modelo.Informe1Model;
import restful.service.Informe1Service;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("Informe1Resource")
public class Informe1ResourceResource {

    Informe1Service informe = new Informe1Service();
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Informe1Model> getPerfiles() {
        
        return informe.obtenerInforme();
    }

    
}
