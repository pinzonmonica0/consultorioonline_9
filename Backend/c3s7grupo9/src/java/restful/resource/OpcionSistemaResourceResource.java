/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.OpcionSistemaModel;
import restful.service.OpcionSistemaService;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("OpcionSistema")
public class OpcionSistemaResourceResource {

    OpcionSistemaService opcion = new OpcionSistemaService();
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<OpcionSistemaModel> getOpcionesSistema() {
        
        return opcion.obtenerOpcionesSistema();
    }

    @Path("/{OpcionId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public OpcionSistemaModel getOpcionSistema(@PathParam("OpcionId") String id){
        return opcion.obtenerOpcionSistema(id);
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public OpcionSistemaModel addOpcionSistema(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        OpcionSistemaModel modelo = gson.fromJson(JSON, OpcionSistemaModel.class);
        
        return opcion.insertarOpcionSistema(modelo);
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public OpcionSistemaModel updateOpcionSistema(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        OpcionSistemaModel modelo = gson.fromJson(JSON, OpcionSistemaModel.class);
        
        return opcion.modificarOpcionSistema(modelo);
    }
}
