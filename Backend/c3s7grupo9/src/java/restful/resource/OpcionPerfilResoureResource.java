/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import restful.modelo.OpcionPerfilModel;
import restful.service.OpcionPerfilService;

/**
 * REST Web Service
 *
 * @author frendon
 */
@Path("OpcionPerfil")
public class OpcionPerfilResoureResource {
    
    OpcionPerfilService opcion = new OpcionPerfilService();
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<OpcionPerfilModel> getOpcionesPerfiles() {
        
        return opcion.obtenerOpcionesPerfiles();
    }
    
    @Path("/{PerfilId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<OpcionPerfilModel> getOpcionesPerfil(@PathParam("PerfilId") String idPerfil){
        return opcion.obtenerOpcionesPerfil(idPerfil);
    }
    
    

    @Path("/{PerfilId}/{OpcionId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public OpcionPerfilModel getOpcionPerfil(@PathParam("PerfilId") String idPerfil,@PathParam("OpcionId") String idOpcion){
        return opcion.obtenerOpcionPerfil(idPerfil,idOpcion);
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public OpcionPerfilModel addOpcionSistema(String JSON)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        OpcionPerfilModel modelo = gson.fromJson(JSON, OpcionPerfilModel.class);
        
        return opcion.insertarOpcionPerfil(modelo);
    }
    
   
   
}
