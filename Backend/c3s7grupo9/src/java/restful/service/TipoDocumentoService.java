
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.TipoDocumentoModel;

/**
 *
 * @author frendon
 */
public class TipoDocumentoService {
    
    public ArrayList<TipoDocumentoModel> obtenerTiposDocumento() {
        ArrayList<TipoDocumentoModel> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT * FROM tipos_de_documento";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                TipoDocumentoModel tipo_documento = new TipoDocumentoModel();
                tipo_documento.setId_tipo_documento(rs.getString("Id_tipo_de_documento"));
                tipo_documento.setTipo_de_documento(rs.getString("tipo_de_documento"));
                lista.add(tipo_documento);
            }
        } catch (Exception e) {
        }
        conex.desconectar();
        return lista;
    }
    
    
     public TipoDocumentoModel obtenerTipoDocumento(String id){
        TipoDocumentoModel tipo_documento = new TipoDocumentoModel();
        Conexion conex = new Conexion();
        String sql  = "SELECT * FROM tipos_de_documento WHERE Id_tipo_de_documento = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {                
                tipo_documento.setId_tipo_documento(rs.getString("Id_tipo_de_documento"));
                tipo_documento.setTipo_de_documento(rs.getString("tipo_de_documento"));
                
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return tipo_documento;
    }
    
}
