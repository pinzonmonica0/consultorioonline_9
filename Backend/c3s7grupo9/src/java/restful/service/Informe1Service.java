/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.Informe1Model;

/**
 *
 * @author frendon
 */
public class Informe1Service {
    public ArrayList<Informe1Model> obtenerInforme() {
        ArrayList<Informe1Model> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT concat_ws('',B.id_beneficiario,B.tipo_documento) AS documento,"
                + "CONCAT_WS('',B.nombre ,B.apellido) as nombre,B.telefono1,B.email1"
                + " FROM agenda_citas C INNER JOIN beneficiarios B ON C.Beneficiario=B.id_beneficiario"
                + " WHERE NOT C.FechaHoraAtencion IS NULL";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Informe1Model fila = new Informe1Model();
                fila.setDocumento_beneficiario(rs.getString("documento"));
                fila.setNombre_beneficiario(rs.getString("nombre"));
                fila.setTelefono(rs.getString("telefono1"));
                fila.setDireccion(rs.getString("email1"));
                lista.add(fila);
            }
        } catch (Exception e) {
        }
        conex.desconectar();
        return lista;
    }
    
}
