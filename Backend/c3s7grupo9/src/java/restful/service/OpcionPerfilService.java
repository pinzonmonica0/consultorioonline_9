/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restful.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import restful.modelo.Conexion;
import restful.modelo.OpcionPerfilModel;


/**
 *
 * @author frendon
 */
public class OpcionPerfilService {
    public ArrayList<OpcionPerfilModel> obtenerOpcionesPerfiles() {
        ArrayList<OpcionPerfilModel> lista = new ArrayList<>();

        Conexion conex = new Conexion();
        String sql = "SELECT * FROM opciones_perfil";

        try {
            Statement stm = conex.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                OpcionPerfilModel opcion = new OpcionPerfilModel();
                opcion.setPerfil(rs.getString("Perfil"));
                opcion.setOpcion(rs.getString("Opcion"));
                
                lista.add(opcion);
            }
        } catch (Exception e) {
        }
        conex.desconectar();
        return lista;
    }
    
    public OpcionPerfilModel obtenerOpcionPerfil(String idPerfil,String idOpcion){
        OpcionPerfilModel opcion = new OpcionPerfilModel();
        Conexion conex = new Conexion();
        String sql  = "SELECT * FROM opciones_perfil WHERE Perfil = ? AND Opcion=?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, idPerfil);
            pstm.setString(2, idOpcion);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                opcion.setPerfil(rs.getString("Perfil"));
                opcion.setOpcion(rs.getString("Opcion"));                               
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return opcion;
    }
    
    public ArrayList<OpcionPerfilModel> obtenerOpcionesPerfil(String idPerfil){
        ArrayList<OpcionPerfilModel> lista = new ArrayList<>();
        Conexion conex = new Conexion();
        String sql  = "SELECT * FROM opciones_perfil WHERE Perfil = ?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, idPerfil);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                OpcionPerfilModel opcion = new OpcionPerfilModel();
                opcion.setPerfil(rs.getString("Perfil"));
                opcion.setOpcion(rs.getString("Opcion"));
                
                lista.add(opcion);
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
        }
        conex.desconectar();
        return lista;
    }
     
     
    public String eliminarOpcionPerfil(String idPerfil,String idOpcion)
    {
        Conexion conex = new Conexion();
        String sql = "DELETE FROM opciones_perfil WHERE Perfil = ? AND Opcion=?";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            pstm.setString(1, idPerfil);
            pstm.setString(2, idOpcion);
            pstm.executeUpdate();
        } catch (Exception e) {
            return "{\"Accion\":\"Error\"}";
        }
        
        
        conex.desconectar();
        return "{\"Accion\":\"Registro Borrado\"}";
    }
    
    public OpcionPerfilModel insertarOpcionPerfil(OpcionPerfilModel opcion){
        Conexion conex = new Conexion();
        String sql = "INSERT INTO opciones_perfil values(?,?)";
        
        try {
            PreparedStatement pstm = conex.getCon().prepareStatement(sql);
            
            pstm.setString(1, opcion.getPerfil());
            pstm.setString(2, opcion.getOpcion());

            
            pstm.executeUpdate();
        } catch (Exception e) {
            return null;
        }
        
        conex.desconectar();
        return opcion;
    }
    
    
}
